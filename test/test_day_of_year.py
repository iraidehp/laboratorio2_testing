from day_of_year import day_of_year, InvalidArgument
import pytest

def test_A():
    with pytest.raises(InvalidArgument):
        assert day_of_year(), "Function called with invalid arguments"

def test_B():
    with pytest.raises(InvalidArgument):
        assert day_of_year(32, 3, 2019), "Function called with invalid arguments"

def test_C():
    with pytest.raises(InvalidArgument):
        assert day_of_year(31, 11, 1878), "Function called with invalid arguments"

def test_D():
    with pytest.raises(InvalidArgument):
        assert day_of_year(8, 15, 2015), "Function called with invalid arguments"

def test_E():
    with pytest.raises(InvalidArgument):
        assert day_of_year(8, 1, -2015), "Function called with invalid arguments"

def test_F():
    with pytest.raises(InvalidArgument):
        assert day_of_year(8), "Function called with invalid arguments"

def test_G():
    with pytest.raises(InvalidArgument):
        assert day_of_year(8.8, 6, 2015), "Function called with invalid arguments"

def test_H():
    with pytest.raises(InvalidArgument):
        assert day_of_year(-3,2,2008), "Function called with invalid arguments"

def test_I():
    with pytest.raises(InvalidArgument):
        assert day_of_year(36,2,2001), "Function called with invalid arguments"

@pytest.mark.parametrize("day,month,year,expected", [(5, 5, 1999, 125), (15, 3, 2024, 75)])
def test_max_dist(day, month, year, expected):
    assert day_of_year(day, month, year) == expected