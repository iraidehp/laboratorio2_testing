from dos_menores import dos_menores

def test_A():
    assert dos_menores() == None

def test_B():
    assert dos_menores([]) == None

def test_C():
    assert dos_menores([3]) == (3)

def test_D():
    assert dos_menores(1) == None

def test_E():
    assert dos_menores([4,10]) == (4,10)

def test_F():
    assert dos_menores([5,2]) == (2,5)

def test_G():
    assert dos_menores([5,3,7,9,1]) == (1,3)

def test_H():
    assert dos_menores([5,1,7,9,3]) == (1,3)