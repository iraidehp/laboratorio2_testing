from max_dist import max_dist, InvalidArgument
import pytest

def test_A():
    with pytest.raises(InvalidArgument):
        assert max_dist(), "Function called with invalid arguments"

def test_B():
    with pytest.raises(InvalidArgument):
        assert max_dist([1]), "Function called with invalid arguments"

def test_C():
    with pytest.raises(InvalidArgument):
        assert max_dist(3), "Function called with invalid arguments"

def test_D():
    with pytest.raises(TypeError):
        assert max_dist([6,1.5])

def test_E():
    with pytest.raises(TypeError):
        assert max_dist([3, 6, 8, 4.5])

@pytest.mark.parametrize("test_input,expected", [([1,6], 5), ([7,3],4), ([3,9,12],6), ([7,6,2],4), ([8,-5,7],13),
	([6,37,1], 36), ([10, 50, 10], 40), ([15, -1, 60], 61)])

def test_max_dist(test_input, expected):
    assert max_dist(test_input) == expected

