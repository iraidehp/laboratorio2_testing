import pytest
class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass

def bisiesto(y):
    return y % 4 == 0 and (y % 100 != 0 or y % 400 == 0)

def day_of_year(day=None, month=None, year=None):
    if day==None or month==None or year==None: #1
        raise InvalidArgument
    elif month < 1 or month > 12 or year < 1: #4
        raise InvalidArgument
    elif type(day)!= int or type(month)!= int or type(year)!= int: #5
        raise InvalidArgument
    elif month in [1, 3, 5, 7, 8, 10, 12] and (day < 1 or day > 31): #2
        raise InvalidArgument
    elif month in [4,6,9,11] and (day < 1 or day > 30): #3
        raise InvalidArgument
    elif bisiesto(year) and month == 2 and (day < 1 or day > 29): #6
        raise InvalidArgument
    elif not bisiesto(year) and month == 2 and (day < 1 or day > 28): #7
        raise InvalidArgument

    if bisiesto(year):
        dias = [31,29,31,30,31,30,31,31,30,31,30,31] #9
    else:
        dias = [31,28,31,30,31,30,31,31,30,31,30,31] #8

    i=0
    num_dias = 0
    while i < (month-1):
        num_dias += dias[i]
        i += 1
    num_dias += day
    return num_dias
