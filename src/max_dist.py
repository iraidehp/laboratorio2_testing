class InvalidArgument(Exception):
    "Function called with invalid arguments"
    pass

def max_dist(a=None):
    if type(a) != list or len(a) < 2:   #1
        raise InvalidArgument

    n1 = a[0]
    n2 = a[1]
    dist = abs(n1-n2)

    if type(n1)!= int or type(n2)!=int: #2
        raise TypeError

    for i in range(2, len(a)):
        if type(a[i]) != int:       #3
            raise TypeError
        else:
            d_aux = abs(a[i]-n2) #4
            print(d_aux)
            if d_aux > dist:
                dist = d_aux
            n2 = a[i]

    return dist


